# JeedomDotNet
JeedomDotNet is an experimental DotNet client library for Jeedom, written in C#.

**THIS REPOSITORY IS NOW DEPRECATED !!!**

Please use this repository instead : **[https://github.com/prudho/JeedomDotNet](https://github.com/prudho/JeedomDotNet)**
